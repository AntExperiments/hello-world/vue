import {Joke} from "@/models/joke";
import {ActionTree, GetterTree, MutationTree} from "vuex";
import {jokeApi} from "@/api/jokeApi";
import Vue from 'vue';

export interface JokeState {
    jokes: Joke[];
    ratings: number[];
}

const state = {
    jokes: [],
    ratings: []
} as JokeState;

const getters = {
    getJokes: (state) => state.jokes,
    getCount: (state) => state.jokes.length,
} as GetterTree<JokeState, any>;

const actions = {

    async loadJoke({commit, dispatch}, excludeChuckNorris) {
        dispatch('START_LOADING', 'loadingJoke', {root: true});
        const excludePattern = /(\bChuck\b)|(\bNorris\b)/;
        let joke: Joke;
        let condition: boolean;
        do {
            joke = await jokeApi.getJoke()
                .then(joke => {
                    return joke;
                });
        } while (excludeChuckNorris && !!joke.joke.match(excludePattern));

        commit('ADD_JOKE', joke);
        commit('ADD_RATING', 0)
        dispatch('FINISH_LOADING', 'loadingJoke', {root: true});
        return joke;
    },

    updateRating({ commit }, { jokeIndex, jokeRating }) {
        commit('UPDATE_RATING', { jokeIndex, jokeRating })
    },

    deleteJoke({commit}, jokeIndex: number) {
        commit('DELETE_JOKE', jokeIndex);
    },

    moveUp({state, commit}, jokeIndex: number) {
        if (jokeIndex <= 0) return;

        const joke = state.jokes[jokeIndex];
        const otherJoke = state.jokes[jokeIndex - 1];

        commit('SET_JOKE', {index: jokeIndex, joke: otherJoke});
        commit('SET_JOKE', {index: jokeIndex - 1, joke});
    },

    moveDown({state, commit}, jokeIndex: number) {
        if (jokeIndex >= state.jokes.length - 1) return

        const joke = state.jokes[jokeIndex];
        const otherJoke = state.jokes[jokeIndex + 1];

        commit('SET_JOKE', {index: jokeIndex, joke: otherJoke});
        commit('SET_JOKE', {index: jokeIndex + 1, joke: joke});
    },

} as ActionTree<JokeState, any>;

const mutations = {

    ADD_JOKE(state, joke: Joke) {
        state.jokes.push(joke);
    },

    ADD_RATING(state, rating: number) {
        state.ratings.push(rating)
    },

    UPDATE_RATING(state, { jokeIndex, jokeRating }) {
        Vue.set(state.ratings, jokeIndex, jokeRating)
    },

    DELETE_JOKE(state, jokeIndex: number) {
        state.jokes.splice(jokeIndex, 1);
    },

    SET_JOKE(state, {index, joke}: { index: number; joke: Joke }) {
        Vue.set(state.jokes, index, joke);
    },

} as MutationTree<JokeState>;

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
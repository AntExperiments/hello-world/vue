import {AxiosError, AxiosRequestConfig, AxiosResponse} from "axios";
import {Joke} from "@/models/joke";
import Api from "@/api/api";

class JokeApi extends Api {
    public constructor(config: AxiosRequestConfig) {
        super(config);
    }

    public getJoke(): Promise<Joke> {
        return this.get<Joke>('', {
            params: {
                format: 'json'
            },
        })
            .then((response: AxiosResponse<Joke>) => {
                return new Joke(response.data);
            })
            .catch((error: AxiosError) => {
                throw error;
            });
    }
}

export const jokeApi = new JokeApi({});